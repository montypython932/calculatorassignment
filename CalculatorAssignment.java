import javax.swing.JOptionPane;
public class CalculatorAssignment
{
	public static void main(String [] args)
	{
		//Prompts the user for input for five strings, parsed to ints
		int numOne = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number"));
		int numTwo = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number"));
		int numThree = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number"));
		int numFour = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number"));
		int numFive = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number"));

		//Calculates the sum and average of the five ints
		int sum = numOne + numTwo + numThree + numFour + numFive;
		double average = (numOne + numTwo + numThree + numFour + numFive) / 5;

		//Extra credit calculations, find max and min
		int max = Math.max(Math.max(Math.max(numOne, numTwo), Math.max(numThree, numFour)), numFive);
		int min = Math.min(Math.min(Math.min(numOne, numTwo), Math.min(numThree, numFour)), numFive);
		
		//Outputs all the calculations using JOptionPane
		JOptionPane.showMessageDialog(null, "Sum = " + sum + "\nAverage = " + average + "\nMax = " + max + "\nMin = " + min);
		System.exit(0);
	}
}